import React, { useEffect, useState } from 'react';

function LocationForm( ) {
  const [states, setStates] = useState([]);  //new code

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      setName('');
      setRoomCount('');
      setCity('');
      setState('');
    }
  }

  const [name, setName] = useState("");

  function nameChanged(event) {
    console.log(event);
    setName(event.target.value);
  }

  const [roomCount, setRoomCount] = useState("");

  function roomCountChanged(event) {
    console.log(event);
    setRoomCount(event.target.value);
  }

  const [city, setCity] = useState("");

  function cityChanged(event) {
    console.log(event);
    setCity(event.target.value);
  }


  const [state, setState] = useState("");

  function stateChanged(event) {
    console.log(event);
    setState(event.target.value);
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new location</h1>
        <form onSubmit={handleSubmit} id="create-location-form">
          <div className="form-floating mb-3">
            <input value={name} onChange={nameChanged} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input value={roomCount} onChange={roomCountChanged} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
            <label htmlFor="room_count">Room count</label>
          </div>
          <div className="form-floating mb-3">
            <input value={city} onChange={cityChanged} placeholder="City" required type="text" name="city" id="city" className="form-control" />
            <label htmlFor="city">City</label>
          </div>
          <div className="mb-3">
          <select  onChange={stateChanged} required name="state" id="state" className="form-select" defaultValue={"DEFAULT"}>
            <option  value="DEFAULT">Choose a state</option>
            {states.map(state => {
              return (
                <option key={state.abbreviation} value={state.abbreviation} >
                  {state.name}
                </option>
              );
            })}
          </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default LocationForm;
