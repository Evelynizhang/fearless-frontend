function formatDate(date) {
  let d = new Date(date);
  let month =  d.getUTCMonth() + 1;
  let day =  d.getUTCDate();
  let year = d.getFullYear();
  return [month, day, year].join("/");
}


function createCard(name, description, pictureUrl, starts, ends,locationPlace) {
  return `
    <div class="card mb-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle  text-secondary">${locationPlace}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer"> ${formatDate(starts)} - ${formatDate(ends)} </div>
    </div>
  `;

}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error(e);
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      let i = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const locationPlace = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, starts, ends,locationPlace);
          const column = document.querySelector('#col-'+i);
          column.innerHTML += html;
          i = i + 1;
          if (i>2) {
            i = 0;
          }

        }
      }
  }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

});
